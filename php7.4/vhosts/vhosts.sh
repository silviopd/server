#!/bin/bash
rm /etc/apache2/apache2.conf
cp -f /var/www/vhosts/apache2.conf /etc/apache2/apache2.conf

rm /etc/php/7.4/apache2/php.ini
cp -f /var/www/vhosts/php.ini /etc/php/7.4/apache2/php.ini


# virtualhost
cp -f /var/www/vhosts/sites/pacificoriesgos.conf /etc/apache2/sites-available/pacificoriesgos.conf
a2ensite pacificoriesgos.conf

cp -f /var/www/vhosts/sites/academyapi.conf /etc/apache2/sites-available/academyapi.conf
a2ensite academyapi.conf

cp -f /var/www/vhosts/sites/intranetapi.conf /etc/apache2/sites-available/intranetapi.conf
a2ensite intranetapi.conf

cp -f /var/www/vhosts/sites/pyrapi.conf /etc/apache2/sites-available/pyrapi.conf
a2ensite pyrapi.conf

cp -f /var/www/vhosts/sites/pvuland.conf /etc/apache2/sites-available/pvuland.conf
a2ensite pvuland.conf 


# agregando al host
echo "127.0.0.1 pacifico-riesgos-api" >> /etc/hosts
echo "127.0.0.1 academy-api" >> /etc/hosts
echo "127.0.0.1 intranet-api" >> /etc/hosts
echo "127.0.0.1 pvuland" >> /etc/hosts


/etc/init.d/apache2 reload
