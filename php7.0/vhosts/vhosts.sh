#!/bin/bash
rm /etc/apache2/apache2.conf
cp -f /var/www/vhosts/apache2.conf /etc/apache2/apache2.conf

rm /etc/php/7.0/apache2/php.ini
cp -f /var/www/vhosts/php.ini /etc/php/7.0/apache2/php.ini


# virtualhost
cp -f /var/www/vhosts/sites/iteprevengov2.conf /etc/apache2/sites-available/iteprevengov2.conf
a2ensite iteprevengov2.conf 

cp -f /var/www/vhosts/sites/pym_encuesta.conf /etc/apache2/sites-available/pym_encuesta.conf
a2ensite pym_encuesta.conf 

cp -f /var/www/vhosts/sites/itprevengotottus.conf /etc/apache2/sites-available/itprevengotottus.conf
a2ensite itprevengotottus.conf 

cp -f /var/www/vhosts/sites/itprevengo.conf /etc/apache2/sites-available/itprevengo.conf
a2ensite itprevengo.conf 

cp -f /var/www/vhosts/sites/pymcursos.conf /etc/apache2/sites-available/pymcursos.conf
a2ensite pymcursos.conf 

cp -f /var/www/vhosts/sites/itprevengo-reyser.conf /etc/apache2/sites-available/itprevengo-reyser.conf
a2ensite itprevengo-reyser.conf 

cp -f /var/www/vhosts/sites/itprevengo-reyser.conf /etc/apache2/sites-available/itprevengo-reyser.conf
a2ensite itprevengo-reyser.conf 

cp -f /var/www/vhosts/sites/testing.conf /etc/apache2/sites-available/testing.conf
a2ensite testing.conf 


# agregando al host
echo "127.0.0.1 iteprevengov2" >> /etc/hosts
echo "127.0.0.1 pymencuestas" >> /etc/hosts
echo "127.0.0.1 itprevengo-tottus" >> /etc/hosts
echo "127.0.0.1 itprevengo-pym" >> /etc/hosts
echo "127.0.0.1 pymcursos" >> /etc/hosts
echo "127.0.0.1 itprevengo-sesga-reyser" >> /etc/hosts
echo "127.0.0.1 testing" >> /etc/hosts

/etc/init.d/apache2 reload
